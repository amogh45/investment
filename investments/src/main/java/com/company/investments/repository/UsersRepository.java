package com.company.investments.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.company.investments.entity.Users;

public interface UsersRepository extends JpaRepository<Users, Long>{

}
